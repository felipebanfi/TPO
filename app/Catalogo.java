package app;

import Implementaciones.Conjunto;
import TDA.ConjuntoTDA;
import tdas.AutorTDA;
import tdas.LibrosAutorTDA;

public class Catalogo
{
	public static void main(String[] args) 
	{
		
	}
	
	public ConjuntoTDA<String> autoresCuyoLibroDeMenorPrecioEstaEntreRango(AutorTDA autores,
			                                                               double precio1,
			                                                               double precio2)
	{	
		ConjuntoTDA<String> conjunto = new Conjunto<String>();
		
		
		
		return conjunto;
	}
	
	public boolean esteTituloPerteneceAlAutorYTienePrecioMenorAlDado(AutorTDA autores,
			                                                         String titulo,
			                                                         String autor,
			                                                         double precioDado)
	{
		return false;
	}
	
	public boolean libroPerteneceAAutor(AutorTDA autores,String autor,String titulo)
	{
		boolean pertenece = false;
		
		LibrosAutorTDA libros = obtenerLibrosAutor(autores,autor);
		
		if (libros != null)
		{
			pertenece = existeTitulo(libros,titulo);
		}
		
		return pertenece;
	}

	private LibrosAutorTDA obtenerLibrosAutor(AutorTDA autores, String autor)
	{
		if (autores.estaVacio())
		{
			return null;
		}
		else
		{
			if (autores.autor().equals(autor))
			{
				return autores.libros();
			}
			else
			{
				if (autores.autor().compareTo(autor) < 0)
				{
					return obtenerLibrosAutor(autores.hijoDerecho(),autor);
				}
				else
				{
					return obtenerLibrosAutor(autores.hijoIzquierdo(),autor);
				}
			}
		}
	}
	
	private boolean existeTitulo(LibrosAutorTDA libros, String titulo)
	{
		if (libros.estaVacio())
		{
			return false;
		}
		else
		{
			if (libros.titulo().equals(titulo))
			{
				return true;
			}
			else
			{
				if (libros.titulo().compareTo(titulo) < 0)
				{
					return existeTitulo(libros.hijoDerecho(),titulo);
				}
				else
				{
					return existeTitulo(libros.hijoIzquierdo(),titulo);
				}
			}
		}
	}
}
