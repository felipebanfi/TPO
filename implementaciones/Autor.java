package implementaciones;

import tdas.AutorTDA;
import tdas.LibrosAutorTDA;

public class Autor implements AutorTDA 
{
	class Nodo
	{
		String autor;
		AutorTDA hi;
		AutorTDA hd;
		LibrosAutorTDA libros;
	}
	
	private Nodo raiz;
	
	@Override
	public void inicializar()
	{
		raiz = null;
	}

	@Override
	public String autor() 
	{
		return raiz.autor;
	}

	@Override
	public LibrosAutorTDA libros()
	{
		return raiz.libros;
	}

	@Override
	public boolean estaVacio() 
	{
		return raiz == null;
	}

	@Override
	public AutorTDA hijoIzquierdo()
	{
		return raiz.hi;
	}

	@Override
	public AutorTDA hijoDerecho() 
	{
		return raiz.hd;
	}

	@Override
	public void agregar(String autor,String titulo,double precio)
	{
		/*
		 * no existe el autor
		 */
		if (raiz == null)
		{
			raiz = new Nodo();
			raiz.autor = autor;
			raiz.libros = new LibrosAutor();
			raiz.libros.inicializar();
			raiz.libros.agregar(titulo, precio);
			
			raiz.hi = new Autor();
			raiz.hi.inicializar();
			
			raiz.hd = new Autor();
			raiz.hd.inicializar();
		}
		else
		{
			/*
			 * el autor es el que busco
			 * agrego el libro
			 */
			if (autor.equals(raiz.autor))
			{
				raiz.libros.agregar(titulo, precio);
			}
			else
			{
				/*
				 * el autor que busco, est� a la izquierda
				 */
				if (autor.compareTo(raiz.autor) < 0)
				{
					raiz.hi.agregar(autor,titulo,precio);
				}
				else
				{
					/*
					 * el autor que busco, est� a la derecha
					 */
					//autor.compareTo(raiz.autor) > 0
					raiz.hd.agregar(autor,titulo,precio);
				}
			}
		}
	}
}
