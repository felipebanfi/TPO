package implementaciones;

import tdas.LibrosAutorTDA;

public class LibrosAutor implements LibrosAutorTDA
{
	class Nodo
	{
		String titulo;
		double precio;
		LibrosAutorTDA hi;
		LibrosAutorTDA hd;
	}
	
	private Nodo raiz;
	
	@Override
	public void inicializar()
	{
		raiz = null;
	}

	@Override
	public String titulo() 
	{
		return raiz.titulo;
	}

	@Override
	public double precio() 
	{
		return raiz.precio;
	}

	@Override
	public boolean estaVacio() 
	{
		return raiz == null;
	}

	@Override
	public LibrosAutorTDA hijoIzquierdo()
	{
		return raiz.hi;
	}

	@Override
	public LibrosAutorTDA hijoDerecho()
	{
		return raiz.hd;
	}

	@Override
	public void agregar(String titulo, double precio)
	{
		if (raiz == null)
		{
			raiz = new Nodo();
			raiz.titulo = titulo;
			raiz.precio = precio;
			
			raiz.hi = new LibrosAutor();
			raiz.hi.inicializar();
			
			raiz.hd = new LibrosAutor();
			raiz.hd.inicializar();
		}
		else
		{
			if (precio < raiz.precio)
			{
				raiz.hi.agregar(titulo, precio);
			}
			else
			{
				if (precio > raiz.precio)
				{
					raiz.hd.agregar(titulo,precio);
				}
			}
		}
	}

}
