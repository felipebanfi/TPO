package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import implementaciones.Autor;
import tdas.AutorTDA;
import tdas.LibrosAutorTDA;

public class TestAutor 
{

	@Test
	public void testInicializarCreaArbolVacio()
	{
		AutorTDA autores = new Autor();
		autores.inicializar();
		
		assertTrue(autores.estaVacio());
	}

	@Test
	public void testAutor()
	{
		AutorTDA autores = new Autor();
		autores.inicializar();
		
		autores.agregar("Borges", "El Aleph", 349.99);
		autores.agregar("Cortazar", "Rayuela", 500);
		
		assertEquals("Borges", autores.autor());
	}

	@Test
	public void testLibros() 
	{
		AutorTDA autores = new Autor();
		autores.inicializar();
		
		autores.agregar("Borges", "El Aleph", 349.99);
		autores.agregar("Cortazar", "Rayuela", 500);
		
		LibrosAutorTDA libros = autores.libros();
		assertEquals("El Aleph", libros.titulo());
		assertEquals(349.99, libros.precio(),0.1);
	}

		
	@Test
	public void testAgregar() 
	{
		AutorTDA autores = new Autor();
		autores.inicializar();
		
		autores.agregar("Borges", "El Aleph", 349.99);
		autores.agregar("Cortazar", "Rayuela", 500);
		
		assertFalse(autores.estaVacio());
	}

}
