package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import implementaciones.LibrosAutor;
import tdas.LibrosAutorTDA;

public class TestLibrosAutor
{

	@Test
	public void testInicializar()
	{
		LibrosAutorTDA libros = new LibrosAutor();
		libros.inicializar();
		
		assertTrue(libros.estaVacio());
	}

	@Test
	public void testTitulo() 
	{
		LibrosAutorTDA libros = new LibrosAutor();
		libros.inicializar();
		
		libros.agregar("Rayuela", 500);
		libros.agregar("La nada", 5);
		
		assertEquals("Rayuela", libros.titulo());
	}

	@Test
	public void testPrecio() 
	{
		LibrosAutorTDA libros = new LibrosAutor();
		libros.inicializar();
		
		libros.agregar("Rayuela", 500);
		libros.agregar("La nada", 5);
		
		assertEquals(500, libros.precio(),0.1);
	}

		
	@Test
	public void testAgregar() 
	{
		LibrosAutorTDA libros = new LibrosAutor();
		libros.inicializar();
		
		libros.agregar("Rayuela", 500);
		libros.agregar("La nada", 5);
		
		assertFalse(libros.estaVacio());
	}
}
